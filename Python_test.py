from main import measure_calc


def test1():
    assert measure_calc(45, "СМ", "М") == 0.45


def test2():
    assert measure_calc(12, "КМ", "ММ") == 12000000


def test3():
    assert measure_calc(1200, "ММ", "М") == 1.2


def test4():
    assert measure_calc(1200, "М", "КК") == "Ошибка ввода"
