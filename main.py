def measure_calc(Num, sys1, sys2):
    sys1=sys1.upper()
    sys2=sys2.upper()
    file = open("test.txt", "a")
    if (sys1 != "ММ" and sys1 != "СМ" and sys1 != "ДМ" and sys1 != "М" and sys1 != "КМ") or \
            (sys2 != "ММ" and sys2 != "СМ" and sys2 != "ДМ" and sys2 != "М" and sys2 != "КМ"):
        file.write("Ошибка ввода\n")
        return "Ошибка ввода"
    if sys1 == "ММ":
        if sys2 == "СМ":
            a= Num / 10
        if sys2 == "ДМ":
            a= Num / 100
        if sys2 == "М":
            a = Num / 1000
        if sys2== "КМ":
            a = Num / 1000000
    if sys1 == "СМ":
        if sys2 == "ММ":
            a = Num * 10
        if sys2 == "ДМ":
            a = Num / 10
        if sys2 == "М":
            a = Num / 100
        if sys2 == "КМ":
            a = Num / 100000
    if sys1 == "ДМ":
        if sys2 == "ММ":
            a = Num * 100
        if sys2 == "СМ":
            a = Num * 10
        if sys2 == "М":
            a = Num / 10
        if sys2 == "КМ":
            a = Num / 10000
    if sys1 == "М":
        if sys2 == "ММ":
            a = Num * 1000
        if sys2 == "СМ":
            a = Num * 100
        if sys2 == "ДМ":
            a = Num * 10
        if sys2 == "КМ":
            a = Num / 1000
    if sys1 == "КМ":
        if sys2 == "СМ":
            a = Num * 100000
        if sys2 == "ДМ":
            a = Num * 10000
        if sys2 == "М":
            a = Num * 1000
        if sys2 == "ММ":
            a = Num * 1000000
    if sys1 == sys2:
        a = Num

    file.write(str(Num) + " " + sys1 + " = " + str(a) + " " + sys2 + "\n")
    return a

